class Board {
  constructor(windowWidth, windowHeight, pixelWidth) {
    this.windowWidth = windowWidth;
    this.windowHeight = windowHeight;
    this.pixelWidth = pixelWidth;
    this.cols = Math.floor(this.windowHeight / pixelWidth);
    this.rows = Math.floor(this.windowWidth / pixelWidth);
    this.grid = new Array(this.rows);
    for (let i = 0; i < this.grid.length; i++) {
      this.grid[i] = new Array(this.rows);
    }
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        this.grid[i][j] = new Creature();
      }
    }
  }

  draw = () => {
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        let type = this.grid[i][j].type;
        switch (type) {
          case "predator":
            fill(222, 44, 44);
            break;
          case "prey":
            fill(77, 171, 107);
            break;
          case "none":
            fill(51);
            break;
        }
        noStroke();
        rect(
          i * this.pixelWidth,
          j * this.pixelWidth,
          this.pixelWidth,
          this.pixelWidth
        );
      }
    }
  };

  updatePred = (thisCreature, otherCreature) => {
    if (thisCreature.health <= 0) {
      thisCreature.setType(CreatureType.NONE);
      return;
    }

    let otherType = otherCreature.type;

    switch (otherType) {
      case "predator":
        break;
      case "prey":
        otherCreature.setType(CreatureType.PRED);
        thisCreature.heal(otherCreature.health);
        break;
      case "none":
        thisCreature.move(otherCreature);
        break;
    }
  };

  updatePrey = (thisCreature, otherCreature) => {
    let otherType = otherCreature.type;
    let reproduce = false;
    if (thisCreature.health > thisCreature.maxHealth) {
      thisCreature.setHealth(10);
      reproduce = true;
    }

    switch (otherType) {
      case "predator":
        break;
      case "prey":
        break;
      case "none":
        if (reproduce) thisCreature.reproduce(otherCreature);
        else thisCreature.move(otherCreature);
        break;
    }
  };

  up = () => {
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        let thisCreature = this.grid[i][j];
        let type = this.grid[i][j].type;

        if (type == "none") continue;
        let xOff = getRandomInt(-1, 1);
        let yOff = getRandomInt(-1, 1);
        let xAdj = i + xOff;
        let yAdj = j + yOff;

        if (xAdj < 0 || xAdj >= this.rows) continue;
        if (yAdj < 0 || yAdj >= this.cols) continue;

        let otherCreature = this.grid[xAdj][yAdj];

        thisCreature.up();
        switch (type) {
          case "predator":
            this.updatePred(thisCreature, otherCreature);
            break;
          case "prey":
            this.updatePrey(thisCreature, otherCreature);
            break;
        }
      }
    }
  };
}

getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
