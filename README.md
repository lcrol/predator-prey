# pred/ prey Cellular automaton

## rules
-For prey:
    -Tries to move in a random direction.
    -Health increases.
    -When health reaches a threshold:
        -They will reproduce, creating a new "Prey"
        -Their health resets to 1

-For predator:
    -Tries to move in a random direction.
    -Health decreases.
    -When health reaches 0, they die and turn into "Nothing".
    -If the adjacent square is a prey:
        -They will eat it, turning it into a "predator" (reproducing)
        -Their health will increase by the amount of health the eaten prey had

link: https://lcrol.gitlab.io/predator-prey/
