let board;

function setup() {
  let pixelWidth = 10;
  createCanvas(windowWidth, windowHeight);
  b = new Board(1000, 1000, pixelWidth);
}

function draw() {
  b.draw();
  b.up();
}
