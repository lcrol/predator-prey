const CreatureType = {
  PRED: "predator",
  PREY: "prey",
  NONE: "none",
};

class Creature {
  constructor() {
    this.maxHealth = 50;
    this.health = Math.floor(this.maxHealth / 5);
    this.n = Math.random();
    if (this.n > 0.1) this.type = CreatureType.NONE;
    else if (this.n > 0.05) this.type = CreatureType.PREY;
    else this.type = CreatureType.PRED;
  }

  setType = (type) => {
    this.type = type;
  };

  setHealth = (hp) => {
    this.health = hp;
    if (this.health > this.maxHealth) this.health = this.maxHealth;
  };

  heal = (hp) => {
    this.health += hp;
  };

  reproduce = (other) => {
    other.health = 10;
    other.type = CreatureType.PREY;
  };

  move = (other) => {
    other.health = this.health;
    other.type = this.type;
    this.type = CreatureType.NONE;
  };

  up = () => {
    switch (this.type) {
      case "predator":
        this.heal(-1);
        break;
      case "prey":
        this.heal(1);
        break;
      default:
        break;
    }
  };
}
